from django.urls import include, path
from accounts.views import login_view, user_logout, sign_up

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", sign_up, name="signup"),
]
